var settings = require('../../../settings.js');
var blocklistFunctions = require('../blocklist/blocklistFunctions.js');

//!admin blockword hello

module.exports = function(payload, callback){
    var retval = { dm: true };
    if (settings.AdminUsers.indexOf(payload.userID) == -1) {
        retval.messageToSend = "Sorry " + payload.userID + " You are not an admin";
        callback(retval);
        return;
    }
    var command = payload.message.toLowerCase().split(' ')[1];
    var parameter = payload.message.toLowerCase().split(' ')[2];
    switch(command) {
        case "removeblockedword":
            blocklistFunctions.DeleteBlockedWord(parameter, function(message){
                Success(message, callback)
            });
            break;
        case "removeallowdword":
            blocklistFunctions.DeleteAllowedWord(parameter, function(message){
                Success(message, callback)
            });
            break;
        case "blockword":
            blocklistFunctions.BlockWord(parameter, payload, function(message){
                Success(message, callback)
            });
            break;
        case "allowword":
            blocklistFunctions.AllowWord(parameter, payload, function(message){
                Success(message, callback)
            });
            break;
        case "listblocked":
            blocklistFunctions.ListBlocked(function(message){
                Success(message, callback)
            });
            break;
        case "listallowed":
            blocklistFunctions.ListAllowed(function(message){
                Success(message, callback)
            });
            break;
        default:
            retval.messageToSend = "Unknown command " + command;
            break;
    }
}

function Success(message, callback){
    if (message.err) {
        callback({ dm: true, messageToSend: message.err });
        return;
    }
    callback({ dm: true, messageToSend: message.result });
}