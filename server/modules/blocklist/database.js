// HELP: https://www.npmjs.com/package/mongoose

var mongoose = require('mongoose');
var settings = require('../../../settings.js');
var privateSettings = require('../../../privateSettings.js');

if (!privateSettings.dbpass || !privateSettings.dbuser || !settings.dbinstance) {
    throw ("DB SETTINGS NOT CONFIGURED");
}

var connectString = 'mongodb://' + privateSettings.dbuser + ':' + privateSettings.dbpass + '@ds0' + settings.dbinstance + '.mlab.com:' + settings.dbinstance + '/kat'
mongoose.connect(connectString);

var wordSchemea = mongoose.Schema({
    dateAdded: Number,
    word: String,
    addedBy: String
})

var BlockedWords = mongoose.model('BlockedWord', wordSchemea);
var AllowedWords = mongoose.model('AllowedWord', wordSchemea);

module.exports.GetAllBlocked = function(callback) {
    BlockedWords.find(function(err, words) {
        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: words
        });
        return;
    });
}
module.exports.GetAllAllowed = function(callback) {
    AllowedWords.find(function(err, words) {
        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: words
        });
        return;
    });
}

//allowed, blocked, notfound
module.exports.GetWordStatus = function(word, callback) {
    var query = AllowedWords.where({
        word: word
    });
    query.findOne(function(err, found) {
        if (err) {
            callback({
                err: err
            });
            return;
        };
        if (found) {
            callback("ALLOWED");
            return;
        }
        else {
            var query = BlockedWords.where({
                word: word
            });
            query.findOne(function(err, found) {
                if (err) {
                    callback({
                        err: err
                    });
                    return;
                };
                if (found) {
                    callback("BLOCKED");
                    return;
                } else {
                    callback("NOTFOUND");
                    return;
                }
            });
        }
    });
}

module.exports.BlockWord = function(word, username, callback) {
    var blockedWord = new BlockedWords();
    blockedWord.word = word;
    blockedWord.addedBy = username;
    blockedWord.save(function(err) {
        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: word + " blocked"
        });
        return;
    });
}
module.exports.AllowWord = function(word, username, callback) {
    var allowedWord = new AllowedWords();
    allowedWord.word = word;
    allowedWord.addedBy = username;
    allowedWord.save(function(err) {
        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: word + " allowed"
        });
        return;
    });
}

module.exports.DeleteAllowedWord = function(word, callback) {
    AllowedWords.findOneAndRemove({
        word: word
    }, function(err, doc, result) {
        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: word + " removed from allowed"
        });
        return;
    });
}
module.exports.DeleteBlockedWord = function(word, callback) {
    BlockedWords.findOneAndRemove({
        word: word
    }, function(err, doc, result) {

        if (err) {
            callback({
                err: err
            });
            return;
        }
        callback({
            result: word + " removed from blocked"
        });
        return;
    });
}



// module.exports.GetLastN = function (n, callback) {
//     var currentDateValue = new Date().valueOf();
//     var query  = Card.where('dateValue').lt(currentDateValue).sort('-dateValue').limit(n);
//     query.find(function (err, card) {
//         callback(card);
//         return;
//     });
// }

// module.exports.GetUnseen = function (callback) {
//     var currentDateValue = new Date().valueOf();
//     var query  = Card.where('dateValue').gt(currentDateValue).sort('-dateValue');
//     query.find(function (err, card) {
//         callback(card);
//         return;
//     });
// }

// module.exports.Update = function (record, callback) {
//     // console.log('going to update ',record)
//     // Card.where({ _id: record._id }).update(record)

//       Card.findOneAndUpdate({_id: record._id}, record, 
//                          {upsert: true}, function (err) {
//     if (err)  return callback(err);
//       callback('ok')
//       //updateDBCallback(null, 'saved');

//   });

//     // Card.update(record, function (err, raw) {
//     //   if (err) return console.log(err);
//     //   console.log('The raw response from Mongo was ', raw);
//     // });
// }

// module.exports.GetLatestCard = function (callback) {
//     var currentDateValue = new Date().valueOf();
//     var query  = Card.where('dateValue').lt(currentDateValue).sort('-dateValue');
//     query.findOne(function (err, card) {
//         callback(card);
//         return;
//     });
// }

// module.exports.GetCard = function (dateValue, callback) {
//     Card.findOne({ dateValue: dateValue }, function (err, card) {

//         callback(card);
//         return;
//     });
// }