var Database = require("./database.js"),
filter = require("./BadWordsFilter.js");

module.exports.AllowWord = function(word, payload, callback) {
    filter.removeWords(word);
    Database.GetWordStatus(word, function(wordStatus){
        if (wordStatus == "BLOCKED") {
            module.exports.DeleteBlockedWord(word, null);
        }
        if (wordStatus != "ALLOWED") {
            Database.AllowWord(word, payload.user, function(result) {
                if (!callback) return;
                callback(result);
            })
        }
    });
}
module.exports.BlockWord = function(word, payload, callback) {
    filter.addWords(word);  
    
    Database.GetWordStatus(word, function(wordStatus){
        if (wordStatus == "ALLOWED") {
            module.exports.DeleteAllowedWord(word, null);
        }
        if (wordStatus != "BLOCKED") {
            Database.BlockWord(word, payload.user, function(result) {
                if (!callback) return;
                callback(result);
            })
        }
    });
    
}
module.exports.ListBlocked = function(callback) {
    Database.GetAllBlocked(function(result) {
        if (!callback) return;
        callback(result);
    })
}
module.exports.ListBlockedWords = function(callback) {
    Database.GetAllBlocked(function(result) {
        if (!callback) return;
        callback(WordsToArray(result));
    })
}
module.exports.DeleteBlockedWord = function(word, callback) {
    Database.DeleteBlockedWord(word, function(result) {
        if (!callback) return;
        callback(result);
    })
}
module.exports.ListAllowedWords = function(callback) {
    Database.GetAllAllowed(function(result) {
        if (!callback) return;
        callback(WordsToArray(result));
    })
}
module.exports.ListAllowed = function(callback) {
    Database.GetAllAllowed(function(result) {
        if (!callback) return;
        callback(result);
    })
}
module.exports.DeleteAllowedWord = function(word, callback) {
    Database.DeleteAllowedWord(word, function(result) {
        if (!callback) return;
        callback(result);
    })
}
function WordsToArray(words) {
    if (words.err) return words;
    var retval = {result: []};
    for (var i = 0; i < words.result.length; i++){
        retval.result.push(words.result[i].word);
    }
    return retval;
}