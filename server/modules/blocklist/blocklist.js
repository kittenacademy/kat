var blocklistFunctions = require("./blocklistFunctions.js"),
filter = require("./BadWordsFilter.js");

function ReadBlockList(callback){
    blocklistFunctions.ListBlockedWords(function (result){
        if (result.err) {
            console.error(result.err);
            return;
        }
        callback(result.result);
    })
}

function ReadUnBlockList(callback){
    blocklistFunctions.ListAllowedWords(function (result){
        if (result.err) {
            console.error(result.err);
            return;
        }
        callback(result.result);
    })
}

function processFile() {
    var listsParsed = 0;
    ReadUnBlockList(function(unBlockList){
        filter.removeWords(unBlockList);
        listsParsed++;
        //if (listsParsed == 2 ) test();
    });
    
    ReadBlockList(function(blockList){
        filter.addWords(blockList);  
        listsParsed++;
        //if (listsParsed == 2 ) test();
    });
}

//var profanity = require('profanity-util');

module.exports = function(payload){
    var score = 0;
    var message = payload.message.toLowerCase();
    var badwords = CheckBadMessage(message);
    return badwords;
}

function CheckBadMessage(message){
    var filteredMessage = filter.clean(message);
    return {isbad: filteredMessage != message, cleanMessage: filteredMessage, unfiltedMessage: message};
}

processFile();
