var blocklistFunctions = require("./blocklistFunctions")

var word = "zignzagthealiensfromthattvshow";
var payload = {
    user: 'testuser'
};
blocklistFunctions.BlockWord(word, payload, function(result) {
    if (result.err) {
        console.error('BlockWord FAIL', result.err);
    }
    else {
        console.log('BlockWord', result.result);
        blocklistFunctions.ListBlockedWords(function(result) {
            if (result.err) {
                console.error('ListBlocked FAIL', result.err);
            }
            else {
                console.log('ListBlocked', result.result);
                blocklistFunctions.DeleteBlockedWord(word, function(result) {
                    if (result.err) {
                        console.error('DeleteBlockedWord FAIL', result.err);
                    }
                    else {
                        console.log('DeleteBlockedWord', result.result);
                    }
                });
            }
        });
    }
});

blocklistFunctions.AllowWord(word, payload, function(result) {
    if (result.err) {
        console.error('AllowWord FAIL', result.err);
    }
    else {
        console.log('AllowWord', result.result);
        blocklistFunctions.ListAllowedWords(function(result) {
            if (result.err) {
                console.error('ListAllowed FAIL', result.err);
            }
            else {
                console.log('ListAllowed', result.result);

                blocklistFunctions.DeleteAllowedWord(word, function(result) {
                    if (result.err) {
                        console.error('DeleteAllowedWord FAIL', result.err);
                    }
                    else {
                        console.log('DeleteAllowedWord', result.result);
                    }
                });
            }
        });
    }
});