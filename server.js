var Discord = require('discord.io'),
    BlocklistTest = require('./server/modules/blocklist/blocklist.js'),
    DiscordWrap = require('./server/modules/discordwrap/discordwrap.js')
var bot = new Discord.Client({
    token: require('./privateSettings.js').token,
    autorun: true
});

bot.on('ready', function() {
    console.log(bot.username + " - (" + bot.id + ")", "ready");
});
// bot.on("any", function(event) {
// 	console.log(event); //Logs every event
// });
bot.on('message', function(user, userID, channelID, message, event) {
    if (event.d.author.bot) {return;}
    var payload = {user: user, userID: userID, channelID: channelID, message: message, event: event};
    if (message[0] == "!"){
        HandleBotCommand(payload);
    } else {
        CheckForOffensiveMessage(payload);
    }
});

function HandleBotCommand(payload){
    bot.deleteMessage({
        channelID: payload.channelID,
        messageID: payload.event.d.id
    }, function (retval){
        if (retval){ //coouldn't delete message PANIC!
            console.error('ERROR WHEN TRYING TO DELETE MESSAGE', retval);
        }
    });
    var moduleName = payload.message.split(" ")[0].replace("!", "");
    try {
        LoadModule(moduleName, payload);
    }
    catch (err){
        console.warn("Attempted to load " + moduleName + " failed");
        console.warn(err);
    }
}

function LoadModule(moduleName, payload){
    var module = require("./server/modules/"+moduleName+"/"+moduleName+".js");
    module(payload, function(message){
        var to = payload.channelID;
        var messageToSend = "";
        var isDM = false;
        if (typeof message === 'string'){
            messageToSend = message;
        } else {
            messageToSend = message.messageToSend;
            if (message.dm) {
                isDM = true;
                to = payload.userID;
            }
        }
        if (!isDM) {
            messageToSend = "Here's your " + moduleName + " " + payload.user + ": " +messageToSend;
        }
        bot.sendMessage({
                to: to,
                message: messageToSend
        });
    });
}

function CheckForOffensiveMessage(payload){
    var offensiveTest = BlocklistTest(payload);
    if (!offensiveTest.isbad) return;
    bot.deleteMessage({
        channelID: payload.channelID,
        messageID: payload.event.d.id
    }, function (retval){
        if (retval){ //coouldn't delete message PANIC!
            console.error('ERROR WHEN TRYING TO DELETE MESSAGE', retval);
        }
    });
    bot.sendMessage({
        to: payload.userID,
        message: "Hi " + payload.user + " your message was deleted for bad language: " + offensiveTest.cleanMessage + "\r\nPlease don't say that again."
    });
}